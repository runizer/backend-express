import * as knex from "knex";

export async function up(knex: knex) {
  await knex.schema.createTable("issue", t => {
    t.increments("id");
    t.string("title", 1024).notNullable();
    t.text("subject");
    t.integer("created_by").unsigned().notNullable().references("id").inTable("user").onDelete("cascade").onUpdate("cascade");
    t.integer("assigned_to").unsigned().notNullable().references("id").inTable("user").onDelete("cascade").onUpdate("cascade");
    t.timestamp("created_at").defaultTo(knex.fn.now()).notNullable();
  });
}

export async function down(knex: knex) {
  await knex.schema.dropTable("issue");
}
