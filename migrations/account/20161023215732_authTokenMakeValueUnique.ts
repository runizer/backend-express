import * as knex from "knex";

export async function up(knex: knex) {
  await knex.schema.table("authentication_token", t => {
    t.unique(["value"]);
  });
}

export async function down(knex: knex) {
  await knex.schema.table("authentication_token", t => {
    t.dropUnique(["value"]);
  });
}
