import * as knex from "knex";

export async function up(knex: knex) {
  await knex.schema.createTable("user", t => {
    t.increments("id");
    t.string("name", 255).notNullable();
    t.timestamp("created_at").defaultTo(knex.fn.now()).notNullable();
    t.string("login", 255).notNullable();
    t.string("password_hash", 255).notNullable();
    t.unique(["login"]);
  });

  await knex.schema.createTable("authentication_token", t => {
    t.increments("id");
    t.integer("user_id").unsigned().notNullable().references("id").inTable("user").onDelete("cascade").onUpdate("cascade");
    t.timestamp("created_at").defaultTo(knex.fn.now()).notNullable();
    t.string("value", 255).notNullable();
  });
}

export async function down(knex: knex) {
  await knex.schema.dropTable("user");
  await knex.schema.dropTable("authentication_token");
}
