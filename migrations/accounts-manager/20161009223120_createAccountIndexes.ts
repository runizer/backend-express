import * as knex from "knex";

export async function up(knex: knex) {
  await knex.schema.table("account", table => {
    table.unique(["name"]);
  });
}

export async function down(knex: knex) {
  await knex.schema.table("account", table => {
    table.dropUnique(["name"]);
  });
}
