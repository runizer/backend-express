import * as knex from "knex";

export async function up(knex: knex) {
  await knex.schema.createTable("account", table => {
    table.increments("id");
    table.string("name", 255).notNullable();
    table.timestamp("created_at").defaultTo(knex.fn.now()).notNullable();
  });
}

export async function down(knex: knex) {
  await knex.schema.dropTable("account");
}
