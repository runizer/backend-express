// https://github.com/lorenwest/node-config/wiki/Configuration-Files

module.exports = {
  db: {
    // Коннект до базы со списком аккаунтов. Это должен быть объект, который придёт в knex
    // параметры можно глянуть тут: http://knexjs.org/#Installation-client
    // ВАЖНО: это не весь объект с параметрами, а только то, что в клчюе connection, то есть:
    // var knexInstance = knex({
    //   client: 'pg', // нельзя сменить тип бд
    //   connection: {} // тут будет этот объект
    // })
    // interface ConnectionConfig {
    //     host: string;
    //     user: string;
    //     password: string;
    //     database: string;
    //     debug?: boolean;
    // }

    accountManager: {
      host: "localhost",
      user: "postgres",
      database: "runizer_accounts_manager"
    },

    // Коннект до базы аккаунта. Аналогично accountManager, но тут можно использовать переменную
    // $accountName в свойстве database: она заменится (с помощью String.prototype.replace) на реальное
    // название аккаунта.
    // interface ConnectionConfig {
    //     host: string;
    //     user: string;
    //     password: string;
    //     database: string;
    //     debug?: boolean;
    // }
    account: {
      host: "localhost",
      user: "postgres",
      database: "runizer_account_$accountName"
    },

    // Коннект до базы postgres, с помощью которого будут создаваться базы данных и всё такое.
    // Здесь обычно другой пользователь, с правами на DROP DATABASE
    administrator: {
      host: "localhost",
      user: "postgres",
      database: "runizer_accounts_manager",
    }
  }
};
