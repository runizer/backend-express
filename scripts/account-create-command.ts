import {Builder, Handler, Argv} from "yargs";

import {getAdministratorConnect, getAccountsManagerConnect, getAccountDbName} from "src/lib/db/connectsManager";

import {migrateAccount} from "./migration-up-command";

export const command = `account:create <name>`;

export const describe = `Creates a new runizer account`;

export const builder: Builder = yargs => yargs;

interface PassedOptions {
  name: string;
}

export const handler: Handler = async function(argv: Argv & PassedOptions) {
  const accountsManagerConnect = await getAccountsManagerConnect();
  const adminDb = getAdministratorConnect();
  try {
    // 1. check possible account existence
    const possibleAccount = await accountsManagerConnect.select("id").from("account").where({name: argv.name}).limit(1);
    if (possibleAccount.length > 0) {
      throw new Error(`Account ${argv.name} already exists`);
    }
    // 2. Create account database
    const accountDbName = getAccountDbName(argv.name);
    await adminDb.raw("DROP DATABASE IF EXISTS " + accountDbName);
    await adminDb.raw("CREATE DATABASE " + accountDbName);
    console.log("Account database was created successfully");
    console.log("Start migrate that database");
    await migrateAccount(argv.name);
    const newAccount = await accountsManagerConnect.insert({name: argv.name}).into("account").returning("id");
    console.log("account id", newAccount[0]);
  } finally {
    await accountsManagerConnect.destroy();
    await adminDb.destroy();
  }
};
