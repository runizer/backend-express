import "source-map-support/register";

import * as yargs from "yargs";
import * as Bluebird from "bluebird";

import * as MigrationCreate from "./migration-create-command";
import * as MigrationUp from "./migration-up-command";
import * as AccountCreate from "./account-create-command";

function commandCatchHandler(e: any) {
  console.log("Error while command");
  console.log(e);
  process.exit(e.code ? e.code : 1);
}

function interopCommand<T extends yargs.CommandModule>(command: T): T {
  return Object.assign({}, command, {
    handler: function (this: any, ...args: any[]): any {
      return Bluebird.try(() => command.handler.apply(this, args)).catch(commandCatchHandler);
    },
  });
}

export const argv = yargs
  .version(`0.0.1`)
  .command(interopCommand(MigrationCreate))
  .command(interopCommand(MigrationUp))
  .command(interopCommand(AccountCreate))
  .completion()
  .help(`help`)
  .epilog(`(c) Runizer team`)
  .demand(1)
  .strict()
  .argv;
