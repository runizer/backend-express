import {Builder, Handler, Argv} from "yargs";
import {qb} from "src/lib/db/connectsManager";
import * as path from "path";

export const command = `migration:create <name> [table]`;

export const describe = `Creates a new migration with specified name & type`;

export const builder: Builder = yargs =>
  yargs
    .option("type", {
      type: "string",
      choices: ["account", "accounts-manager"],
      default: "account",
    })
  ;

interface PassedOptions {
  name: string;
  type: "account" | "accounts-manager";
  table?: string;
}

export const handler: Handler = async function(argv: Argv & PassedOptions) {
  const config = {
    stub: path.join(process.cwd(), "scripts", "migration.stub"),
    extension: "ts",
    directory: path.join(process.cwd(), "migrations", argv.type),
    variables: {} as any,
  };
  if (argv.table) {
    config.variables.tableName = argv.table;
  }
  await qb.migrate.make(argv.name, config);
  console.log("Migration just created. Don't forget to restart typescript!");
};
