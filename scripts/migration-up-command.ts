import {Builder, Handler, Argv} from "yargs";
import {getAccountsManagerConnect, getAccountConnect} from "src/lib/db/connectsManager";
import * as path from "path";
import * as knex from "knex";

export const command = `migration:up`;

export const describe = `Migrate database of specified type`;

export const builder: Builder = yargs =>
  yargs
    .option("type", {
      type: "string",
      choices: ["account", "accounts-manager"],
      default: "account",
    })
  ;

interface PassedOptions {
  name: string;
  type: "account" | "accounts-manager";
  table?: string;
}

export const handler: Handler = async function(argv: Argv & PassedOptions) {
  if (argv.type === "accounts-manager") {
    await migrateAccountsManager();
  } else {
    await migrateAccounts();
  }
};

async function migrateDb(connect: knex, config: knex.MigratorConfig) {
  const currentVersion = await connect.migrate.currentVersion(config);
  console.log("Current version is:", currentVersion);
  const status = await connect.migrate.status(config);
  if (status === 0) {
    console.log("No new migrations found");
  } else if (status < 0) {
    console.log(String(-1 * status), "new migrations found");
    await connect.migrate.latest(config);
    console.log("All migrations up");
  } else {
    throw new Error("You database has more migrations, than code. It is error, fix it");
  }
}

async function migrateAccountsManager() {
  const config = {
    directory: path.join(process.cwd(), "target", "server", "migrations", "accounts-manager"),
  };
  console.log("Migrate accounts manager");
  const connect = await getAccountsManagerConnect();
  console.log("Successfully connected");
  try {
    await migrateDb(connect, config);
  } finally {
    await connect.destroy();
    console.log("Disconnected from db");
  }
}

async function migrateAccounts() {
  console.log("Migrate accounts");
  // 1. Obtain connect to accountsManager
  const accountsManagerConnect = await getAccountsManagerConnect();
  try {
    // 2. get list of accounts
    const allAccounts: {name: string}[] = await accountsManagerConnect.select("name").from("account");
    console.log("Found accounts:", allAccounts.length);
    // 3. migrate accounts one-by-one
    for (const account of allAccounts) {
      try {
        console.log("Migrate account", account.name);
        await migrateAccount(account.name);
      } catch (e) {
        console.error(`Failed to migrate account ${account.name}:`, e);
      }
    }
  } finally {
    accountsManagerConnect.destroy();
  }
}

export async function migrateAccount(name: string) {
  const config = {
    directory: path.join(process.cwd(), "target", "server", "migrations", "account"),
  };
  // 3.1 Obtain account connect
  const connect = await getAccountConnect(name);
  try {
    await migrateDb(connect, config);
  } finally {
    connect.destroy();
  }
}
