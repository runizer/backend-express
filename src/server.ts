import * as express from "express";
import * as morgan from "morgan";
import {AppKernel} from "src/AppKernel";

const app = new AppKernel();

const server = express();
server.use(morgan(":method :url :handledByAction :status :response-time ms - :res[content-length]"));
server.use("/api", app.router());
morgan.token("handledByAction", app.morganActionTokenHandler);
server.listen(3333, function(err: any) {
  if (err) {
    throw err;
  }
  console.log("Server started. Open http://<accountName>.<domain>.<zone>:3333/");
});
