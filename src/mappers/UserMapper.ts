import {Mapper, PropertyFactory, DbRow} from "src/lib/db/DataMapper";
import {User} from "common/models/User";

export class UserMapper implements Mapper<User> {

  public type = User;
  public tableName = "user";
  public idFieldName = "id";
  public fieldToColumn = {
    "id": "id",
    "name": "name",
    "createdAt": "created_at",
    "login": "login",
    "passwordHash": "password_hash",
  };

  public createInstanceFromDbRow(
    {}: PropertyFactory,
    dbRow: DbRow,
  ): User {
    if (dbRow["id"] && dbRow["name"] && dbRow["created_at"] && dbRow["login"] && dbRow["password_hash"]) {
      const result = new User();
      result.id = Promise.resolve(dbRow["id"]);
      result.name = Promise.resolve(dbRow["name"]);
      result.createdAt = Promise.resolve(dbRow["created_at"]);
      result.login = Promise.resolve(dbRow["login"]);
      result.passwordHash = Promise.resolve(dbRow["password_hash"]);
      return result;
    } else {
      throw new Error("No enough fields to construct User!");
    }
  }
}
