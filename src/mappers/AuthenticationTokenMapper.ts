import {Mapper, PropertyFactory, DbRow} from "src/lib/db/DataMapper";
import {AuthenticationToken} from "common/models/AuthenticationToken";
import {User} from "common/models/User";

export class AuthenticationTokenMapper implements Mapper<AuthenticationToken> {

  public type = AuthenticationToken;
  public tableName = "authentication_token";
  public idFieldName = "id";
  public fieldToColumn = {
    "id": "id",
    "value": "value",
    "createdAt": "created_at",
    "user": "user_id",
  };

  public createInstanceFromDbRow(
    propertyFactory: PropertyFactory,
    dbRow: DbRow,
  ): AuthenticationToken {
    if (dbRow["id"] && dbRow["value"] && dbRow["created_at"] && dbRow["user_id"]) {
      const result = new AuthenticationToken();
      result.id = Promise.resolve(dbRow["id"]);
      result.value = Promise.resolve(dbRow["value"]);
      result.createdAt = Promise.resolve(dbRow["created_at"]);
      propertyFactory.defineRefLink(result, "user", User, dbRow["user_id"] as string | number);
      return result;
    } else {
      throw new Error("No enough fields to construct AuthenticationToken!");
    }
  }
}
