import {Mapper, PropertyFactory, DbRow} from "src/lib/db/DataMapper";
import {Issue} from "common/models/Issue";
import {User} from "common/models/User";

export class IssueMapper implements Mapper<Issue> {

  public type = Issue;
  public tableName = "issue";
  public idFieldName = "id";
  public fieldToColumn = {
    "id": "id",
    "title": "title",
    "subject": "subject",
    "createdAt": "created_at",
    "createdBy": "created_by",
    "assignedTo": "assigned_to",
  };

  public createInstanceFromDbRow(propertyFactory: PropertyFactory, dbRow: DbRow): Issue {
    const result = new Issue();
    result.id = Promise.resolve(dbRow["id"]);
    result.title = Promise.resolve(dbRow["title"]);
    result.subject = Promise.resolve(dbRow["subject"]);
    result.createdAt = Promise.resolve(dbRow["created_at"]);
    propertyFactory.defineRefLink(result, "createdBy", User, dbRow["created_by"] as string | number);
    propertyFactory.defineRefLink(result, "assignedTo", User, dbRow["assigned_to"] as string | number);
    return result;
  }
}
