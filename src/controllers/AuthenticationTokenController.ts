import * as Bluebird from "bluebird";
import * as httpErrors from "http-errors";

import {randomBytes, createHmac} from "crypto";
const randomBytesAsync = Bluebird.promisify(randomBytes);

import {Route, Post, Return, Req} from "src/lib/decorators";
import {DataMapper} from "src/lib/db/DataMapper";
import {AuthenticationToken} from "common/models/AuthenticationToken";
import {Request} from "src/lib/Kernel";

@Route("/AuthenticationToken")
export class AuthenticationTokenController {

  @Post()
  @Return(AuthenticationToken)
  public async createToken(
    dm: DataMapper,
    @Req request: Request,
  ): Promise<AuthenticationToken> {
    const users = await dm.connect
      .select("id")
      .from("user")
      .where({
        login: request.body.login,
        password_hash: dm.connect.raw("md5(?)", request.body.password),
      })
      .limit(1)
    ;
    if (users.length === 0) {
      throw new httpErrors.BadRequest();
    }
    // create the new AuthenticationToken
    const createdTokens = await dm
      .connect("authentication_token")
      .insert({
        user_id: users[0].id,
        value: createHmac("sha256", await randomBytesAsync(128)).digest("hex"),
      })
      .returning("*")
    ;
    const createdToken = createdTokens[0];
    const token = new AuthenticationToken();
    token.id = createdToken.id;
    token.createdAt = createdToken.created_at;
    token.value = createdToken.value;

    console.log(token);

    return token;
  }
}
