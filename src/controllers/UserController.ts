import {Route, Get, Return, Req} from "src/lib/decorators";
import {User} from "common/models/User";
import {AuthenticatedRequest} from "src/lib/Kernel";

@Route("/User")
export class UserController {

  @Get("")
  @Return(User)
  public async getList(
    @Req req: AuthenticatedRequest,
  ): Promise<Array<User>> {
    return [
      await req.token.user,
    ];
  }
}
