import {Route, Get, Post, Return} from "src/lib/decorators";
import {Issue} from "common/models/Issue";
import {DataMapper} from "src/lib/db/DataMapper";

@Route("/Issue")
export class IssueController {

  @Get()
  @Return(Issue)
  public async getList(
    dm: DataMapper,
  ): Promise<Array<Issue>> {
    return dm.all(Issue);
  }

  @Post()
  @Return(Issue)
  public async create(
    dm: DataMapper,
  ): Promise<Issue> {
    return await dm.inTransaction(() => {
      return new Issue();
    });
  }
}
