import {Class, Id} from "./types";

export class IdentityMap {
  private storage = new Map<Class<{}>, Map<Id, {}>>();
  private instances = new Set<{}>();

  private getTypeStorage<T>(type: Class<T>): Map<Id, T> {
    let typeStorage = this.storage.get(type);
    if (!typeStorage) {
      typeStorage = new Map();
      this.storage.set(type, typeStorage);
    }
    return typeStorage as Map<Id, T>;
  }

  public hasInstance = (instance: any): boolean => {
    return this.instances.has(instance);
  }

  public get = <T>(type: Class<T>, id: Id): T | undefined => {
    return this.getTypeStorage(type).get(id);
  }

  public set = <T>(type: Class<T>, id: Id, instance: T) => {
    this.getTypeStorage(type).set(id, instance);
    this.instances.add(instance);
  }
}
