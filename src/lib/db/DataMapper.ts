import * as knex from "knex";
import {Class, ClassWithCriteria, Id} from "./types";
import {IdentityMap} from "./IdentityMap";

type ColumnName = string;
export type DbRow = {[columnName: string]: string | number | Date};
export interface Mapper<T> {
  type: Class<T>;
  tableName: string;
  idFieldName: string;
  fieldToColumn: {[fieldName: string]: ColumnName};
  createInstanceFromDbRow(propertyFactory: PropertyFactory, dbRow: DbRow): T;
}

export interface PropertyFactory {
  defineRefLink<T, C>(target: any, propertyName: string, valueType: ClassWithCriteria<T, C>, valueId: Id): void;
}

const hasOwnProperty = Object.prototype.hasOwnProperty;

export class DataMapper {

  private mappersByType = new Map<Class<{}>, Mapper<{}>>();

  private identityMap = new IdentityMap();

  private valueStorages = new WeakMap<{}, Map<string, any>>();

  private propertyFactory: PropertyFactory;

  private transactionCounter = 0;
  private currentTransaction?: Transaction;

  constructor(
    public connect: knex,
    mappers: Mapper<any>[],
  ) {
    for (const mapper of mappers) {
      this.mappersByType.set(mapper.type, mapper);
    }
    this.propertyFactory = {
      defineRefLink: this.defineRefLink,
    };
  }

  private getMapper<T>(type: Class<T>): Mapper<T> | never {
    const result = this.mappersByType.get(type);
    if (!result) {
      throw new Error("No mapper found for entity class " + type.name);
    }
    return result as Mapper<T>;
  }

  /**
   * Позволяет объявить свойство в модели, которое является рефлинком. Если сама модель ничего не знает про
   * значение рефлинка то триггерится загрузка из БД.
   * @param target
   * @param propertyName
   * @param valueType
   * @param valueId
   */
  private defineRefLink = <T, C>(target: any, propertyName: string, valueType: ClassWithCriteria<T, C>, valueId: Id): void => {
    let storage = this.valueStorages.get(target);
    if (!storage) {
      storage = new Map();
      this.valueStorages.set(target, storage);
    }
    const fStorage = storage;
    Object.defineProperty(target, propertyName, {
      enumerable: true,
      configurable: true,
      get: async () => {
        let result = fStorage.get(propertyName);
        if (!result) {
          result = this.getById(valueType, valueId);
          fStorage.set(propertyName, result);
        }
        return result;
      },
      set: (value: T) => {
        fStorage.set(propertyName, value);
      },
    });
  }

  /**
   * Преобразовывает объект фильтрации из фильтрации по полям в фильтрацию по колонкам.
   * @param mapper
   * @param criteria
   */
  private mapCriteria(mapper: Mapper<any>, criteria: any): any {
    const result: any = {};
    if (null != criteria && typeof criteria === "object") {
      for (const field in criteria) {
        if (hasOwnProperty.call(criteria, field)) {
          result[mapper.fieldToColumn[field]] = criteria[field];
        }
      }
    }
    return result;
  }

  public isManagedInstance = (instance: {}): boolean => {
    return this.identityMap.hasInstance(instance);
  }

  public all = async <T, C>(type: ClassWithCriteria<T, C>, criteria?: C): Promise<Array<T>> => {
    const qb = this.getPreparedQb(type, criteria);
    const rows = await qb.limit(100); // TODO Parametrize limit
    return rows.map((r: DbRow) => this.getInstanceFromDbResult(type, r));
  }

  public getById = async <T, C>(type: ClassWithCriteria<T, C>, id: Id): Promise<T | undefined> => {
    const instance = this.identityMap.get(type, id);
    if (instance) {
      return instance;
    }
    const mapper = this.getMapper(type);
    return this.findOneBy(type, {[mapper.idFieldName]: id} as {});
  }

  public inTransaction = async <R>(cb: () => R | Promise<R>): Promise<R> => {
    this.currentTransaction = new Transaction(this.transactionCounter++, this.currentTransaction);
    await this.currentTransaction.begin(this.connect);
    try {
      const cbRes = await cb();
      await this.currentTransaction.commit(this.connect);
      this.currentTransaction = this.currentTransaction.previous;
      return cbRes;
    } catch (e) {
      await this.currentTransaction.rollback(this.connect);
      throw e;
    }
  }

  /**
   *
   * @param type
   * @param criteria
   */
  public findOneBy = async <T, C>(type: ClassWithCriteria<T, C>, criteria: C): Promise<T | undefined> => {
    const qb = this.getPreparedQb(type, criteria);
    return qb
      .limit(1)
      .then((rows: DbRow[]) => {
        if (rows.length > 0) {
          return this.getInstanceFromDbResult(type, rows[0]);
        }
      })
    ;
  }

  private getPreparedQb<T, C>(type: ClassWithCriteria<T, C>, criteria: C) {
    const mapper = this.getMapper(type);
    return this.connect
      .select(Object.values(mapper.fieldToColumn))
      .from(mapper.tableName)
      .where(this.mapCriteria(mapper, criteria))
    ;
  }

  private getInstanceFromDbResult<T>(type: Class<T>, dbRow: DbRow): T {
    const mapper = this.getMapper(type);
    const id = String(dbRow[mapper.fieldToColumn[mapper.idFieldName]]);
    if (id) {
      let instance = this.identityMap.get(type, id);
      if (!instance) {
        instance = mapper.createInstanceFromDbRow(this.propertyFactory, dbRow);
        this.identityMap.set(type, id, instance);
      }
      return instance;
    } else {
      throw new Error(`There is no expected "${mapper.fieldToColumn[mapper.idFieldName]}" column returned from db!`);
    }
  }
}

class Transaction {
  private savepointName: string;

  constructor(counter: number, public previous?: Transaction) {
    this.savepointName = `transaction_${counter}`;
  }

  public async begin(connect: knex) {
    if (this.previous) {
      await connect.raw(`SAVEPOINT ${this.savepointName}`);
    } else {
      await connect.raw("BEGIN");
    }
  }

  public async commit(connect: knex) {
    if (!this.previous) {
      await connect.raw("COMMIT");
    }
  }

  public async rollback(connect: knex) {
    if (this.previous) {
      await connect.raw(`ROLLBACK TO SAVEPOINT ${this.savepointName}`);
    } else {
      await connect.raw("ROLLBACK");
    }
  }
}
