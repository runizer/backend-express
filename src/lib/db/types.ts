export type Class<T> = {
  new(...args: any[]): T;
};
export type ClassWithCriteria<T, C> = Class<T> & {
  criteria: C;
};
export type Id = string | number;
