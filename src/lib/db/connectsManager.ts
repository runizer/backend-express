import * as knex from "knex";
import * as config from "config";

export interface Connect extends knex {
}

export function getAdministratorConnect() {
  return knex({
    client: "pg",
    connection: config.get<knex.ConnectionConfig>("db.administrator"),
  });
}

export function getAccountsManagerConnect() {
  return knex({
    client: "pg",
    connection: config.get<knex.ConnectionConfig>("db.accountManager"),
    pool: {
      min: 1,
      max: 5,
    },
  });
}

export function getAccountConnect(accountName: string) {
  const dbConfig = config.get<knex.ConnectionConfig>("db.account");
  return knex({
    client: "pg",
    connection: Object.assign({}, dbConfig, {
      database: dbConfig.database.replace("$accountName", accountName),
    }),
  });
}

export function getAccountDbName(accountName: string) {
  return config.get<knex.ConnectionConfig>("db.account").database.replace("$accountName", accountName);
}

export const qb = knex({client: "pg"});
