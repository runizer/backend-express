export type HttpMethod = "GET" | "POST" | "PUT" | "DELETE";

const routeMetadataKey = Symbol("routeMetadataKey");

interface RouteMetadata {
  path: string;
  method?: HttpMethod;
}

export function getRouteMetadata(target: any, propertyKey?: string): RouteMetadata | undefined {
  if (propertyKey) {
    return Reflect.getMetadata(routeMetadataKey, target, propertyKey);
  } else {
    return Reflect.getMetadata(routeMetadataKey, target);
  }
}

export function Route(path: string): ClassDecorator;
export function Route(path: string, method?: HttpMethod): MethodDecorator;
export function Route(path: string, method?: HttpMethod): ClassDecorator | MethodDecorator {
  const metadata: RouteMetadata = {path, method};
  return (target: any, propertyKey?: string) => {
    if (propertyKey && typeof target[propertyKey] !== "function") {
      throw new Error("Can not use @Route decorator on non-function");
    }
    if (propertyKey) {
      Reflect.defineMetadata(routeMetadataKey, metadata, target, propertyKey);
    } else {
      Reflect.defineMetadata(routeMetadataKey, metadata, target);
    }
  };
}

export function Get(path = "") {
  return Route(path, "GET");
}

export function Post(path = "") {
  return Route(path, "POST");
}

export function Put(path = "") {
  return Route(path, "PUT");
}

export function Delete(path = "") {
  return Route(path, "DELETE");
}
