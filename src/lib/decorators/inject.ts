export const Request = Symbol("Request");
export const Response = Symbol("Response");

const injectMetadataKey = Symbol("inject");
const isMetadataCompleted = Symbol("isMetadataCompleted");

/**
 * Функция инжектит сервисы в контроллер.
 * Используется как декоратор над параметром
 */
export function inject(what?: any) {
  return function(target: Object, propertyKey: string | symbol, parameterIndex: number) {
    const existingInjectMetadata: {[index: number]: boolean} =
      Reflect.getOwnMetadata(injectMetadataKey, target, propertyKey) || Object.create(null);
    if (parameterIndex in existingInjectMetadata) {
      throw new Error(`You can not use @inject twice on same parameter ${propertyKey} on ${target} at parameter ${parameterIndex}`);
    }
    existingInjectMetadata[parameterIndex] = what;
    Reflect.defineMetadata(injectMetadataKey, existingInjectMetadata, target, propertyKey);
  };
}

export const Req = inject(Request);
export const Res = inject(Response);

export function getInjectMetadata(target: Object, propertyKey: string | symbol): {[index: number]: any} {
  const injectMetadata: any = Reflect.getMetadata(injectMetadataKey, target, propertyKey) || Object.create(null);
  if (isMetadataCompleted in injectMetadata) {
    return injectMetadata;
  }
  const designParamTypes: any[] = Reflect.getMetadata("design:paramtypes", target, propertyKey) || [];
  if (designParamTypes.length > 0) {
    designParamTypes.forEach((v, i) => {
      if (void 0 === injectMetadata[i]) {
        injectMetadata[i] = v;
      }
    });
  }
  injectMetadata[isMetadataCompleted] = true;
  return injectMetadata;
}
