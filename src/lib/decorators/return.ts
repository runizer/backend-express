const returnMetadataKey = Symbol("returnMetadata");

type Constructor = new(...args: any[]) => any;
type PossibleReturns = Constructor | number | string | boolean;

export function Return<T extends PossibleReturns>(what: T): MethodDecorator {
  return function(target: Object, propertyKey: string | symbol) {
    Reflect.defineMetadata(returnMetadataKey, what, target, propertyKey);
  };
}
