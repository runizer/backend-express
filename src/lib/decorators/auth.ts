const skipAuthMetadataKey = Symbol("skipAuth");

export function isAuthSkipped(target: Object, propertyKey: string | symbol) {
  return Boolean(Reflect.getMetadata(skipAuthMetadataKey, target, propertyKey));
}

export const SkipAuth: MethodDecorator = function SkipAuth(target: Object, propertyKey: string | symbol) {
  Reflect.defineMetadata(skipAuthMetadataKey, true, target, propertyKey);
};
