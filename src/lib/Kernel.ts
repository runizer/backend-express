import * as express from "express";
import * as bodyParser from "body-parser";
import * as methodOverride from "method-override";
import * as httpErrors from "http-errors";
import * as apiErrorHandler from "api-error-handler";

import {AuthenticationToken} from "common/models/AuthenticationToken";

import {getInjectMetadata, Request as InjectRequest, Response as InjectResponse} from "./decorators/inject";
import {getRouteMetadata, HttpMethod} from "./decorators/route";
import {isAuthSkipped} from "./decorators/auth";
import {DataMapper, Mapper} from "./db/DataMapper";
import {Connect, getAccountsManagerConnect, getAccountConnect, qb} from "./db/connectsManager";

type ControllerClass = {
  new<T>(): T,
};

export type Request = express.Request & {
  handledByAction?: string;
  canBeHandledBy?: HttpMethod[];
  token?: AuthenticationToken;
  dm: DataMapper;
  action: {
    controller: any,
    methodName: string,
    arguments: any[],
  }
};
export type AuthenticatedRequest = Request & {
  token: AuthenticationToken;
};
type Response = express.Response & {
  actionResult?: any,
};
type NextFunction = express.NextFunction;

interface AsyncRequestHandler {
  (req: express.Request, res: express.Response, next: express.NextFunction): Promise<any>;
}
function wrap(fn: AsyncRequestHandler): express.Handler {
  return function(req, res, next) {
    fn(req, res, next).catch(next);
  };
}

/**
 * Класс приложения для апи.
 * Здесь происходит установка всей бизнес-логики связанной с апи рунайзера.
 * Регистрируются все маршруты и обработчики этих маршрутов.
 */
export class Kernel {
  /**
   * Корневой роутер для всех апи-маршрутов
   */
  private rootRouter: express.Router;

  /**
   * Классы контроллеров, где необходимо искать маршруты
   */
  private controllerClasses: ControllerClass[];

  /**
   * Существующие дата-мапперы, группированные по имени аккаунта.
   * @type {{}}
   */
  private dataMappers: {[accountName: string]: DataMapper | undefined} = {};

  /**
   * Коннект до базы с аккаунтами
   */
  private accountsManagerConnect: Connect;

  constructor(controllers: ControllerClass[], private mappers: Mapper<any>[]) {
    this.controllerClasses = controllers;
    this.rootRouter = express.Router();
    this.accountsManagerConnect = getAccountsManagerConnect();
  }

  /**
   * Сетапит роутер
   */
  public router(): express.Router {
    this.rootRouter.use(methodOverride());
    this.rootRouter.use(bodyParser.json());
    this.setupActions();
    this.rootRouter.use(this.sendActionResult);
    this.rootRouter.use(this.failIfNoResult);
    this.rootRouter.use(apiErrorHandler());
    return this.rootRouter;
  }

  /**
   * Сетапит все маршруты приложения
   */
  private setupActions(): void {
    for (let controllerClass of this.controllerClasses) {
      const router: express.Router = express.Router(); // на каждый контролер - отдельный роутер
      let controllerMountPath = ""; // префикс всех маршрутов контроллера
      // Префикс всех маршрутов может задаваться аннотацией @Route над классом
      const classRouteMetadata = getRouteMetadata(controllerClass);
      if (classRouteMetadata) {
        controllerMountPath = classRouteMetadata.path;
      }
      const controllerInstance: any = new controllerClass();
      const controllerPrototype = controllerClass.prototype;
      for (let propertyName of Object.getOwnPropertyNames(controllerPrototype)) {
        const methodRouteMetadata = getRouteMetadata(controllerPrototype, propertyName);
        if (methodRouteMetadata) {
          // для каждого маршрута:
          router.all(
            methodRouteMetadata.path,
            // 1. Достать коннект до БД
            this.populateAccountDataMapper,
            // 2. Определить кто его будет обрабатывать
            wrap(async (req: Request, res: Response, next: NextFunction) => {
              if (methodRouteMetadata.method && req.method !== methodRouteMetadata.method) {
                if (req.canBeHandledBy) {
                  req.canBeHandledBy.push(methodRouteMetadata.method);
                } else {
                  req.canBeHandledBy = [methodRouteMetadata.method];
                }
                next("route");
                return;
              }
              req.handledByAction = controllerClass.name + "." + propertyName;
              req.action = {
                controller: controllerInstance,
                methodName: propertyName,
                arguments: await this.resolveControllerArgs(
                  req,
                  res,
                  controllerPrototype,
                  propertyName,
                ),
              };
              next();
            }),
            // аутентифицировать пользователя если необходимо
            this.populateRequestToken,
            // запустить сам экшен
            this.runAction,
          );
        }
      }
      this.rootRouter.use(controllerMountPath, router);
    }
  }

  /**
   * Perform resolving of controller arguments.
   * @param req
   * @param res
   * @param controllerPrototype
   * @param methodName
   */
  private async resolveControllerArgs(
    req: Request,
    res: Response,
    controllerPrototype: any,
    methodName: string | symbol,
  ): Promise<any[]> {
    const method: Function = controllerPrototype[methodName];
    const expectedLength = method.length;
    const injectMetadata = getInjectMetadata(controllerPrototype, methodName);
    const args: {}[] = [];
    for (let i = 0; i < expectedLength; i++) {
      const ExpectedClass = injectMetadata[i];
      if (ExpectedClass === DataMapper) {
        args.push(req.dm);
      } else if (ExpectedClass === InjectRequest) {
        args.push(req);
      } else if (ExpectedClass === InjectResponse) {
        args.push(res);
      } else {
        throw new Error(`Unsupported ${i} parameter for ${controllerPrototype.constructor.name}.${methodName}`);
      }
    }
    return args;
  }

  public morganActionTokenHandler = (req: Request) => {
    return req.handledByAction ? req.handledByAction : "";
  }

  private sendActionResult = wrap(async (req: Request, res: Response, next: NextFunction) => {
    if (!req.handledByAction) {
        next();
      } else {
        if (typeof res.actionResult === undefined) {
          next(new Error(`Action ${req.handledByAction} returns undefined`));
        } else {
          // normalize response, resolve promises;
          if (res.actionResult instanceof Array) {
            res.json(
              await Promise.all(
                res.actionResult.map((model: any) => this.normalizeModel(req.dm, model)),
              ),
            );
          }
          res.json(await this.normalizeModel(req.dm, res.actionResult));
        }
      }
  });

  private async normalizeModel(dm: DataMapper, model: {[index: string]: any}): Promise<{}> {
    const result: {[index: string]: any} = {"@type": model.constructor.name};
    await Promise.all(Object.keys(model).map(async key => {
      result[key] = await model[key];
      if (dm.isManagedInstance(result[key])) {
        result[key] = {
          "@type": result[key].constructor.name,
          id: await result[key].id,
        };
      }
    }));
    return result;
  }

  private failIfNoResult = wrap(async (req: Request, res: Response, next: NextFunction) => {
    if (req.canBeHandledBy) {
      res.set("Allow", req.canBeHandledBy.join(","));
      next(new httpErrors.MethodNotAllowed());
    } else {
      next(new httpErrors.NotFound());
    }
  });

  private populateAccountDataMapper = wrap(async (req: Request, {}: Response, next: NextFunction) => {
    const accountName = req.hostname.substr(0, req.hostname.indexOf("."));
    if (this.dataMappers[accountName]) {
      req.dm = this.dataMappers[accountName]!;
      next();
      return;
    }
    // тут мы проверим аккаунт
    const accountRow = await this.accountsManagerConnect
      .select(qb.raw("true"))
      .from("account")
      .where({name: accountName})
      .limit(1)
    ;
    if (accountRow.length === 0) {
      return next(new httpErrors.NotFound());
    }
    if (!this.dataMappers[accountName]) {
      this.dataMappers[accountName] = new DataMapper(getAccountConnect(accountName), this.mappers);
    }
    req.dm = this.dataMappers[accountName]!;
    next();
  });

  private populateRequestToken = wrap(async (req: Request, {}: Response, next: NextFunction) => {
    if (isAuthSkipped(req.action.controller.constructor.prototype, req.action.methodName)) {
      next();
    } else {
      // токен приходит в заголовке Authorization в сехеме bearer (на предъявителя).
      const auth = req.get("authorization");
      if (auth) {
        const splitter = auth.indexOf(" ");
        const scheme = auth.substr(0, splitter);
        if (scheme === "Bearer") {
          const token = auth.substr(splitter).trim();
          const tokenModel = await req.dm.findOneBy(AuthenticationToken, {value: token});
          if (tokenModel) {
            req.token = tokenModel;
            next();
            return;
          }
        }
      }
      next(new httpErrors.Unauthorized());
    }
  });

  private runAction = wrap(async (req: Request, res: Response, next: NextFunction) => {
    try {
      res.actionResult = await req.action.controller[req.action.methodName](...req.action.arguments);
      next();
    } catch (e) {
      next(e);
    }
  });
}
