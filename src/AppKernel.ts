import {Kernel} from "./lib/Kernel";

import {UserController} from "./controllers/UserController";
import {AuthenticationTokenController} from "./controllers/AuthenticationTokenController";
import {IssueController} from "./controllers/IssueController";

import {AuthenticationTokenMapper} from "./mappers/AuthenticationTokenMapper";
import {UserMapper} from "./mappers/UserMapper";
import {IssueMapper} from "./mappers/IssueMapper";

export class AppKernel extends Kernel {
  constructor() {
    super(
      [
        UserController,
        AuthenticationTokenController,
        IssueController,
      ],
      [
        new AuthenticationTokenMapper(),
        new UserMapper(),
        new IssueMapper(),
      ],
    );
  }
}
