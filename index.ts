import "source-map-support/register";
import {resolve, join} from "path";
/* tslint:disable:no-var-requires */
require("app-module-path").addPath(__dirname);
require("app-module-path").addPath(resolve(join(__dirname, "..")));
/* tslint:enable:no-var-requires */
import "reflect-metadata";
import "babel-polyfill";

// main file
import "src/server";
